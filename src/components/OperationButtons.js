import React from 'react'
import { Button } from "@progress/kendo-react-buttons";

function OperationButtons(props) {
    const generateData = () => {
        props.handleRequest("/generate")
    }
    const finishDay = () => {
        props.handleRequest("/finishDay")
    }
    const finishWeek = () => {
        props.handleRequest("/finishWeek")
    }

    return (
        <div className="buttons_container">
            <div className="prize_pool">
                <b>Prize Pool: </b>
                <span>{props.prizePool}</span>
            </div>
            <Button className="button-custom" onClick={generateData}>
                Generate New Data
            </Button>
            <Button className="button-custom" onClick={finishDay}>
                Finish The Day
            </Button>
            <Button className="button-custom" onClick={finishWeek} >
                Finish The Week
            </Button>
        </div>
    )
}

export default OperationButtons
