import React from 'react'
import { Grid, GridColumn as Column } from "@progress/kendo-react-grid";
import '@progress/kendo-theme-default/dist/all.css';

function KendoTable(params) {
    const countryFlags = (props) => {
        const countryCode = props.dataItem.country ?? "tr" // insert mehter marsi here
        return (
            <td>
                <img style={{ margin: "auto", display: "block", width:"80%" }} src={`/flags/${countryCode}.png`} />
            </td>
        )
    }

    const renderMoneyCell = (props) => {
        let index = props.dataIndex
        if (props.dataItem.weekly_prize != 0 && index < 100) {
            return (
                <td style={{ display: "flex", justifyContent: "space-between" }}>
                    <b>{props.dataItem.money}</b>
                    <span style={{ color: "green" }}>
                        +{props.dataItem.weekly_prize}
                    </span>
                </td>
            )
        } else {
            return (
                <td>
                    {props.dataItem.money}
                </td>
            )
        }
    }

    // Puts ranks to Top100 && Puts ranks to UserRange with User.rank state
    const getRanks = (props) => {
        const rank = props.dataItem.rank ?? props.dataIndex + 1
        return <td>{rank}</td>
    }

    // Renders the rows with style
    const rowRender = (trElement, props) => {
        const isCurrent = (props.dataItem.name == params.currentUser.name);

        const trProps = {
            style: isCurrent ? {
                background: "green",
                color: "white",
            } : {}
        };

        return React.cloneElement(
            trElement,
            { ...trProps },
            trElement.props.children
        )
    }
    const customHeader = (trElement, props) => {
        const blue = "#5b72da"
        const trProps = {
            style: {
                color: blue,
            }
        };
        return React.cloneElement(
            trElement,
            { ...trProps },
            trElement.props.children
        )
    }
    const renderDailyDiff = (props) => {
        let diff = props.dataItem.daily_diff
        let color = "orange";
        if (diff > 0) {
            color = "green"
        } else if (diff < 0) {
            color = "red"
        }
        return <td style={{ color: color }}>{diff}</td>

    }

    return (
        <div>
            <Grid
                rowRender={rowRender}
                data={params.users}
                headerCellRender={customHeader}
                className="kendo-grid-custom"
            >
                <Column width="60%" field="rank" title="Rank" cell={getRanks} />
                <Column width="60%" field="country" title="Country" cell={countryFlags} />
                <Column field="name" title="Name" />
                <Column field="money" title="Score" cell={renderMoneyCell} />
                <Column field="daily_diff" title="Daily Diff" cell={renderDailyDiff} />
            </Grid>
        </div>
    )
}

export default KendoTable
