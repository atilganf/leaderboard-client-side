import "../assets/css/index.css"
import React, { useState, useEffect } from 'react';
import KendoTable from './KendoTable';
import OperationButtons from './OperationButtons';
import axios from 'axios';


function Leaderboard() {
    const serverURI = "/api";

    const [currentUser, setCurrentUser] = useState()
    const [users, setUsers] = useState([])
    const [topUsers, setTopUsers] = useState([])
    const [progress, setProgress] = useState(true)
    const [prizePool, setPrizePool] = useState(0);

    const loadingPanel = (
        <div className="k-loading-mask">
            <span className="k-loading-text">Loading</span>
            <div className="k-loading-image"></div>
            <div className="k-loading-color"></div>
        </div>
    );

    const getLeaderboard = () => {
        setProgress(true)
        axios.get(`${serverURI}/getLeaderboard`).then(res => {
            let newUser = res.data.user;
            console.log("currentUser rank", newUser.rank)
            console.log("getLeaderboard res.data", res.data)

            let newUsers = res.data.range;
            newUsers.forEach((el, ind) => {
                el.rank = newUser.rank + ind - (newUsers.length - 3)
            })
            console.log("users", res.data.range)

            setCurrentUser(newUser);
            setUsers(newUsers);
            setTopUsers(res.data.topUsers);
            setPrizePool(res.data.prizePool);
        }).then(_ => {
            setProgress(false);
        })
    }

    useEffect(() => {
        handleRequest("/generate")
    }, [])

    const handleRequest = (url) => {
        axios.get(serverURI + url).then(res => {
            console.log(url, res.data)
            getLeaderboard()
        }).catch(err => {
            console.log(err);
        })

    }

    return (
        <div>
            <OperationButtons handleRequest={handleRequest} prizePool={prizePool} />
            {!progress ? <KendoTable
                users={[...topUsers, ...users]}
                currentUser={currentUser}
                prizePool={prizePool}
            /> : loadingPanel}
        </div>
    )
}

export default Leaderboard
