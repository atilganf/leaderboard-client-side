import React, { Component, useState, useEffect } from 'react';
import { Grid, GridColumn as Column } from "@progress/kendo-react-grid";
import '@progress/kendo-theme-default/dist/all.css';

import axios from 'axios';

function OldVersion() {
    const newVersion = true;

    const serverURI = "http://localhost:3002"

    const [currentUser, setCurrentUser] = useState()
    const [users, setUsers] = useState([])
    const [topUsers, setTopUsers] = useState([])
    const [progress, setProgress] = useState(false)
    const [prizePool, setPrizePool] = useState(0);

    const getLeaderboard = () => {
        axios.get(`${serverURI}/getLeaderboard`).then(res => {
            let newUser = res.data.user;
            console.log("currentUser rank", newUser.rank)
            console.log("getLeaderboard res.data", res.data)
            let newUsers = res.data.range;
            newUsers.forEach((el, ind) => {
                el.rank = newUser.rank + ind - (newUsers.length - 3)
            })

            setCurrentUser(newUser);
            setUsers(newUsers);
            setTopUsers(res.data.topUsers);
            setPrizePool(res.data.prizePool);
            setProgress(true)
        })
    }

    // Runs once to get topUsers and randomUser with range
    useEffect(() => {
        getLeaderboard();
    }, [])


    const generateData = () => {
        axios.get(`${serverURI}/generate`)
            .then((res) => {
                console.log("Generate Data");
                console.log(res.data);
                getLeaderboard();
            })
            .catch((err) => {
                console.log(err)
            })
    }
    const finishDay = () => {
        axios.get(`${serverURI}/finishDay`).then(res => {
            console.log("finishDay prizePool", res.data)
            setPrizePool(res.data);
            getLeaderboard();
        })
        console.log("Finish Day")
    }
    const finishWeek = () => {
        axios.get(`${serverURI}/finishWeek`).then(res => {
            console.log("finishWeek", res.data)
            setPrizePool(0);
            getLeaderboard();
        })

    }


    //############# Kendo React Grid Functions
    const countryFlags = (props) => {
        const countryCode = props.dataItem.country ?? "tr" // insert mehter marsi here
        return (
            <td>
                <img src={`https://www.countryflags.io/${countryCode}/shiny/24.png`} />
            </td>
        )
    }

    const renderMoneyCell = (props) => {
        let index = props.dataIndex
        if (props.dataItem.weekly_prize != 0 && index < 100) {

            return (
                <td>
                    <b>{props.dataItem.money}</b>
                    <span style={{ color: "green" }}>
                        ({props.dataItem.weekly_prize})
                    </span>
                </td>
            )
        } else {
            return (
                <td>
                    {props.dataItem.money}
                </td>
            )
        }
    }

    // Puts ranks to Top100 && Puts ranks to UserRange with User.rank state
    const getRanks = (props) => {
        // console.log(props.dataItem)
        const rank = props.dataItem.rank ?? props.dataIndex + 1
        return <td>{rank}</td>
    }

    // Renders the rows with style
    const rowRender = (trElement, props) => {
        const isCurrent = props.dataItem.name == currentUser.name;
        const green = {
            backgroundColor: "rgb(55, 180, 0,0.32)",
        };
        const red = {
            backgroundColor: "rgb(243, 23, 0, 0.32)",
        };
        const trProps = {
            style: isCurrent ? green : red,
        };

        return React.cloneElement(
            trElement,
            { ...trProps },
            trElement.props.children
        )
    }




    return progress ? (
        <div className="App">
            <br />
            < div style={{ display: "flex", justifyContent: "space-around" }
            }>

                <div> <b>PrizePool: </b><span>{prizePool}</span></div>
                <button onClick={generateData}>Generate New Data</button>
                <button onClick={finishDay}>Finish The Day</button>
                <button onClick={finishWeek}>Finish The Week</button>
            </div >
            <br />
            <Grid data={[...topUsers, ...users]} rowRender={rowRender}>
                <Column field="country" width="50px" title=" " cell={countryFlags} />
                <Column field="name" title="Name" />
                <Column field="money" title="Score" cell={renderMoneyCell} />
                <Column field="rank" title="Rank" cell={getRanks} />
                <Column field="daily_diff" title="Daily Difference" />
            </Grid>
        </div >
    ) : "Loading"
}

export default OldVersion;
