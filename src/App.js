import Leaderboard from './components/Leaderboard';
import Header from './components/Header';

function App() {

  return (
    <div className="App">
      <Header/>
      <Leaderboard/>
    </div>
  )
}

export default App;
