# Leaderboard Frontend

## Tech Stack
The project uses:
* [React.js](https://reactjs.org/) for fronted
* [Kendo UI](https://www.telerik.com/kendo-react-ui/) for styling
* [Axios](https://axios-http.com/) for api requests

## Components
- ``Header.js``  
- ``Leaderboard.js``: Holds the states, makes the requests
- ``KendoTable``: Includes the Kendo React Grid Table
- ``Operation Buttons``: Holds 3 operation buttons 